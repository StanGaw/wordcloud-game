import React from "react";
import { useHistory } from "react-router-dom";
import Layout from "../../components/layout/Layout";
import { playerAtom } from "../../atoms/playerAtom";
import { useRecoilValue } from "recoil";
import styles from "./styles.module.css";
import Button from "../../components/button/Button";
const ScoreBoard = () => {
  const history = useHistory();
  const player = useRecoilValue(playerAtom);
  const restart = () => {
    history.push("/");
  };

  return (
    <Layout>
      <div className={styles.scoreboard}>
        <p>Congratulations, {player.nick} !</p>
        <p> Your Score : </p>
        <p className={styles.score}>{player.score}</p>
        <Button onClick={() => restart()}>Restart</Button>
      </div>
    </Layout>
  );
};

export default ScoreBoard;
