import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import styles from "./styles.module.css";
import Layout from "../../components/layout/Layout";
import { playerAtom } from "../../atoms/playerAtom";
import { wordsAtom } from "../../atoms/wordsAtom";
import { useRecoilState } from "recoil";
import data from "../../data/data";
import { getRandomInt, position } from "../../functions/functions";
import Button from "../../components/button/Button";

const Login = () => {
  const history = useHistory();
  const [player, setPlayer] = useRecoilState(playerAtom);
  const [nick, setNick] = useState("");
  const [words, setWords] = useRecoilState(wordsAtom);
  const random = getRandomInt(0, data.length);
  function intializeGameWords() {
    const question = [...data[random].question];
    const allWords = [...data[random].allWords];
    const correctWords = [...data[random].good_words];
    let positions = allWords.map((el) => position());

    setWords({
      ...words,
      question,
      allWords,
      correctWords,
      positions,
    });
  }

  const changeHandler = (e) => {
    setNick(e.target.value);
  };

  const redirect = () => {
    history.push("/game");
  };

  const startTheGame = (e) => {
    e.preventDefault();
    setPlayer({
      ...player,
      nick,
    });
    intializeGameWords();
    redirect();
  };

  return (
    <Layout>
      <div className={styles.loginWrapper}>
        <h2 className={styles.heading}>WordCloud Game</h2>
        <form onSubmit={(e) => startTheGame(e)} className={styles.inputWrapper}>
          <input
            value={nick}
            onChange={(e) => changeHandler(e)}
            name="nickname"
            className={styles.loginInput}
            placeholder="Enter your nickname here..."
          ></input>

          <Button disabled={nick === ""}>Play</Button>
        </form>
      </div>
    </Layout>
  );
};

export default Login;
