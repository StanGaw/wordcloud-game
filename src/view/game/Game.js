import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { wordsAtom } from "../../atoms/wordsAtom";
import { playerAtom } from "../../atoms/playerAtom";
import { useRecoilState } from "recoil";
import Layout from "../../components/layout/Layout";
import styles from "./styles.module.css";
import Button from "../../components/button/Button";

const Game = () => {
  const history = useHistory();

  const [score, setScore] = useState(0);
  const [words, setWords] = useRecoilState(wordsAtom);
  const [player, setPlayer] = useRecoilState(playerAtom);
  const [chosen, setChecked] = useState([]);
  const [show, setShow] = useState(false);
  const { correctWords, allWords, positions, question } = words;

  const scoreCalculate = () => {
    const match = [];
    let wrong = [];
    chosen.forEach((item) => {
      if (correctWords.includes(item)) {
        match.push(item);
      } else {
        wrong.push(item);
      }
    });

    correctWords.forEach((item) => {
      chosen.forEach((el) => {
        if (item !== el) {
          wrong.push(item);
        }
      });
    });

    wrong = wrong.filter((el) => match.includes(el) === false);
    wrong = new Set(wrong);
    wrong = [...wrong];

    let score = match.length * 2 - wrong.length;
    setScore(score);
  };

  const checkHandler = (e) => {
    const target = e.target;
    !show && target.parentElement.classList.toggle("checked");
    const clicked = e.target.innerText;

    setChecked([...chosen, clicked]);
    if (chosen.includes(clicked)) {
      setChecked(chosen.filter((el) => el !== clicked));
    }
  };

  const showScoreBoard = () => {
    history.push("/score-board");
  };
  const showAnswers = () => {
    setPlayer({
      ...player,
      score,
    });
    setShow(true);
  };

  useEffect(() => {
    scoreCalculate();
  }, [chosen]);

  return (
    <Layout>
      <div className={styles.wrapper}>
        <h2 className={styles.wrapper__heading}>{question}</h2>
        <div className={styles.cloud}>
          {allWords.length > 0 &&
            allWords.map((word, id) => {
              return (
                <div
                  style={positions[id]}
                  className={styles.cloud__item}
                  key={word}
                >
                  <p
                    onClick={(e) => {
                      checkHandler(e);
                    }}
                    className={styles.single_word}
                  >
                    {word}
                  </p>
                  {show && (
                    <div className={styles.words_check}>
                      {correctWords.includes(word) ? (
                        <p className={styles.correct}>Good</p>
                      ) : (
                        <p className={styles.incorrect}>Bad</p>
                      )}
                    </div>
                  )}
                </div>
              );
            })}
        </div>
        <div className={styles.buttonWrapper}>
          {show ? (
            <Button onClick={() => showScoreBoard()}>Show the score !</Button>
          ) : (
            <Button
              disabled={chosen.length === 0}
              onClick={() => showAnswers(true)}
            >
              Check Answers
            </Button>
          )}
        </div>
      </div>
    </Layout>
  );
};

export default Game;
