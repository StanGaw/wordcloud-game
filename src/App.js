import {BrowserRouter as Router , Switch, Route} from 'react-router-dom';
import Login from './view/login/Login'
import Game from './view/game/Game'
import ScoreBoard from './view/scoreBoard/ScoreBoard'
function App() {
  return (
   <Router>
     <Switch>
       <Route exact path="/"><Login/></Route>
       <Route  path="/game"><Game/></Route>
       <Route  path="/score-board"><ScoreBoard/></Route>
     </Switch>
   </Router>
    
  );
}

export default App;
