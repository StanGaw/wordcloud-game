export function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}

export const position = () => {
  const left = getRandomInt(0, 50);
  const top = getRandomInt(0, 50);
  const right = getRandomInt(0, 50);
  const bottom = getRandomInt(0, 50);
  const style = {
    marginLeft: left,
    marginTop: top,
    marginRight: right,
    marginBottom: bottom,
  };

  return style;
};
