import { atom } from "recoil";

export const playerAtom = atom({
  key: "player", // unique ID (with respect to other atoms/selectors)
  default: {
    nick: "",
    score: 0,
  }, // default value (aka initial value)
});
