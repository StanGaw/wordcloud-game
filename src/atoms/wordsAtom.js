import { atom } from "recoil";

export const wordsAtom = atom({
  key: "words",
  default: {
    question: "",
    allWords: [],
    correctWords: [],
    positions: [],
  },
});
