import React from "react";
import styles from "./styles.module.css";
const Button = ({ children, onClick, disabled = false }) => {
  return (
    <button onClick={onClick} disabled={disabled} className={styles.playButton}>
      {children}
    </button>
  );
};

export default Button;
