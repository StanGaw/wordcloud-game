#WordCloud Game 

To start the project, install node modules with the npm install script and then run the project using :
    - npm start 
or 
    - yarn script.
In default the Local Server starts at localhost:3000

#About the game.

The player gives the nickname and then, immediately after approval, a set of words from a specific category is drawn, the player's task is to match the matching words.

Once the check answers is clicked Players score is submited and display on the scoreboard.